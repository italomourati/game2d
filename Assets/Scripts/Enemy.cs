﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // VELOCIDADE DO MOVIMENTO
    public float moveSpeed = 5f;
    // DISTANCIA MAXIMA PERCORRIDA
    public int maxDistance = 100;

    // DISTANCIA PERCORRIDA
    private int travelledDistance = 0;
    // DIRENCAO DO INIMIGO
    private bool direction = true;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector2 movement = new Vector2(Time.fixedDeltaTime * moveSpeed, 0f);

        transform.Translate(ChangeDirectionMove() * movement);

        CheckDistanceTravelled();

        travelledDistance += 1;
    }

    private Vector2 ChangeDirectionMove()
    {
        if (direction)
        {
            return Vector2.right;
        }
        else
        {
            return Vector2.left;
        }
    }

    private void CheckDistanceTravelled()
    {
        if (travelledDistance == maxDistance && direction)
        {
            travelledDistance = 0;
            direction = false;
        }

        if (travelledDistance == maxDistance && !direction)
        {
            travelledDistance = 0;
            direction = true;
        }
    }

}
