﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bulletPrefab;

    public AudioSource audioSource;
    public AudioClip tiro;
    public float volume = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    void Shoot()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject game = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Destroy(game, 3.0f);
            audioSource.PlayOneShot(tiro, volume);
        }
    }
}
