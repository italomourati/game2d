﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

    public float moveSpeed = 5f;

    public float jumpForce = 5f;

    private new Rigidbody2D rigidbody2D;

    private bool jump = true;

    public static int COUNT_COIN;

    private bool flip = false;

    // AUDIO
    public AudioSource audioSource;
    public AudioClip tiro;
    public float volume = 0.5f;

    //Animacao
    public Animator animator;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        rigidbody2D.freezeRotation = true;

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }

    void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.fixedDeltaTime * moveSpeed;

        animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal") * moveSpeed));

        Flip(Input.GetAxis("Horizontal"));
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && jump)
        {
            rigidbody2D.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
            animator.SetBool("Jump", true);
        }
    }

    void Flip(float horizontalDirection)
    {
        if (horizontalDirection > 0)
        {
            if (flip)
            {
                transform.Rotate(0, 180, 0);
                flip = false;
            }
        }
        else if (horizontalDirection < 0)
        {
            if (!flip)
            {
                transform.Rotate(0, 180, 0);
                flip = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            jump = true;
            animator.SetBool("Jump", true);
        }
        else if (collision.collider.tag == "Coin")
        {
            COUNT_COIN++;
            Destroy(collision.gameObject);
            Score.scoreValue = COUNT_COIN;
            audioSource.PlayOneShot(tiro, volume);
        }
        else if (collision.collider.tag == "Enemy")
        {
            LifePlayer.LIFE--;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            jump = false;
            animator.SetBool("Jump", false);
        }
    }

}
