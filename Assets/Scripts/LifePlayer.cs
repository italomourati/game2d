﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifePlayer : MonoBehaviour
{

    public static int LIFE = 3;

    Animator animator;

    private bool playerDead = false;

    public GameObject playerGameObject;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (LIFE == 2)
        {
            animator.SetInteger("Life", 2);
        }
        else if (LIFE == 1)
        {
            animator.SetInteger("Life", 1);
        }
        else if (LIFE == 0)
        {
            if (!playerDead)
            {
                Destroy(playerGameObject);
                animator.SetInteger("Life", 0);
                SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
                playerDead = true;
                LIFE = 3;
                Score.scoreValue = 0;
                MovePlayer.COUNT_COIN = 0;
                //CameraFollowPlayer.StopGame();
            }
        }
    }
}
